from api import app
from api import data

app.config['TEMPLATES_AUTO_RELOAD'] = True

if __name__ == "__main__":
    app.run()
    data.to_csv("/data/wordlist.csv")