import random
from flask import Flask, render_template
from flask_cors import CORS, cross_origin
import pandas
import numpy as np
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


data = pandas.read_csv("/data/wordlist.csv", encoding="utf-8", index_col=[0])
wordlist_length = len(data.index)
data["freq"] = [0 if np.isnan(f) else f for f in data["freq"]]

max_bonus = 5

print(data)


@app.route("/api/v1/genword")
@cross_origin()
def random_word():
    index = random.randint(1, wordlist_length)
    freq = data["freq"][index]
    mean = np.mean(data["freq"])
    max = np.amax(data["freq"])
    max_deviation = np.sqrt((max - mean)**2)
    scaler = max_bonus / max_deviation
    deviation = np.sqrt((freq - mean)**2) * scaler
    print(f"maxDeviation:\t{max_deviation}\n"
          f"deviation:\t{deviation}")
    return {"word": data["words"][index].strip("\n"),
            "score": 1 + (min(max_bonus, deviation) if freq > mean else 0)}


@app.route("/api/v1/skip/<word>")
@cross_origin()
def correct_word(word):
    index = data.index[data["words"] == word].tolist()[0]
    val = data.at[index, "freq"]
    print(val)
    if np.isnan(val):
        val = 1
    else:
        val += 1
    data.at[index, "freq"] = val
    return word


@app.route("/index.html")
def index():
    return render_template("/index.html")


if __name__ == "__main__":
    app.run(host="0.0.0.0")
    data.to_csv("/data/wordlist.csv")
